import numpy as np
import logging

from ai4med.common.constants import ImageProperty
from ai4med.common.medical_image import MedicalImage
from ai4med.common.shape_format import ShapeFormat
from ai4med.common.transform_ctx import TransformContext
from ai4med.utils.dtype_utils import str_to_dtype
from ai4med.components.transforms.multi_field_transformer import MultiFieldTransformer

class ScaleImages(MultiFieldTransformer):
    def __init__(self, fields, dtype="float32"):
        MultiFieldTransformer.__init__(self, fields=fields)
        self._dtype = str_to_dtype(dtype)

    def transform(self, transform_ctx: TransformContext):
      for field in self.fields:
        img = transform_ctx.get_image(field)
        data = img.get_data()
        modality = img.get_property("Modality")
        
        if (modality=="CT"):
          ct_thresh_lower = -350
          ct_thresh_upper = 350
        elif (modality=="PT"):
          ct_thresh_lower = 0
          ct_thresh_upper = 10
        else:
          return transform_ctx
        
        data[ct_normalized<ct_thresh_lower] = ct_thresh_lower
        data[ct_normalized>+ct_thresh_upper] = ct_thresh_upper
        data = (ct_normalized-ct_thresh_lower)/(ct_thresh_upper-ct_thresh_lower)
        
        return transform_ctx

