
from utils.DataUtils import DataUtils
from scipy.ndimage import zoom
import cv2

def mask2RT(mask, maskOrigin, PixeSpacing):  
  contoursList = []
  
  contourSequence = []
  for i in range(mask.shape[2]):
    imgSlice = mask[:, :, i]
    
    sliceMask = imgSlice.astype("uint8")
    sliceMask[sliceMask>0] = 2
    
    ret, thresh_img = cv2.threshold(sliceMask, 1, 255, cv2.THRESH_BINARY)

    contours, hierarchy = cv2.findContours(thresh_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    contourData = []
    for j in range(len(contours)):
      subContour = contours[j]
      
      for k in range(len(subContour)):
        subSubContour = subContour[k]
        
        for l in range(len(subSubContour)):
          point = subSubContour[l]
          
          contourData.append(point[1]*PixeSpacing[1]+maskOrigin[1])
          contourData.append(point[0]*PixeSpacing[0]+maskOrigin[0])
          contourData.append(maskOrigin[2]-i*PixeSpacing[2])
    
    contourSequence.append({"ContourData": contourData, "NumberOfContourPoints": len(contourData)/3})

  return contourSequence
  
