
import os
from scipy.ndimage import zoom
from bson.json_util import dumps
import numpy as np
import copy

class DataUtils:
  def __init__(self):
    self.data = []
    self.meta = []
  
  def appendDicomSeries(self, directory):
    #TODO
    print('TODO')
    
  def appendDicomData(self, data, metaData):
    self.data.append(data)
    self.meta.append(metaData)
  
  # TODO static method for crop (like what I did for resample)
  def cropAll(self, x1, x2, y1, y2, z1, z2):
    for i, dataItem in enumerate(self.data):
      meta = copy.deepcopy(self.meta[i])
      Slices = len(self.meta)
      
      indexX1 = round((x1-meta[0]["ImagePositionPatient"][0])/meta[0]["PixelSpacing"][0])
      indexX2 = round((x2-meta[0]["ImagePositionPatient"][0])/meta[0]["PixelSpacing"][0])
      indexY1 = round((y1-meta[0]["ImagePositionPatient"][1])/meta[0]["PixelSpacing"][1])
      indexY2 = round((y2-meta[0]["ImagePositionPatient"][1])/meta[0]["PixelSpacing"][1])
      indexZ1 = round((meta[0]["ImagePositionPatient"][2]-z2)/meta[0]["SliceThickness"])
      indexZ2 = round((meta[0]["ImagePositionPatient"][2]-z1)/meta[0]["SliceThickness"])
        
      newX1 = indexX1*meta[0]["PixelSpacing"][0]+meta[0]["ImagePositionPatient"][0]
      newY1 = indexY1*meta[0]["PixelSpacing"][1]+meta[0]["ImagePositionPatient"][1]
            
      dataItem2 = copy.deepcopy(dataItem)
      self.data[i] = dataItem2[int(indexY1):int(indexY2), int(indexX1):int(indexX2), int(indexZ1):int(indexZ2)]
      
      newShape = self.data[i].shape
      
      newMeta = []
      
      dataInd = 0
      for j, metaItem in enumerate(meta):
        if (meta[j]["ImagePositionPatient"][2]>=z1 and meta[j]["ImagePositionPatient"][2]<=z2):
          newMetaTmp = copy.deepcopy(meta[j])
          
          newMetaTmp["ImagePositionPatient"][0] = newX1
          newMetaTmp["ImagePositionPatient"][1] = newY1
          newMetaTmp["Rows"] = newShape[1]
          newMetaTmp["Columns"] = newShape[0]
          
          newMeta.append(newMetaTmp)
          
          dataInd = dataInd + 1
        
      self.meta[i] = newMeta

  def getData(self):
    return self.data
    
  def getMeta(self):
    return self.meta
  
  def resampleAll(self, newSpacing):
    for i, dataItem in enumerate(self.data): 
           
      resampleRes = self.resample(self.meta[i], dataItem, newSpacing)
        
      self.meta[i] = resampleRes["meta"]
      self.data[i] = resampleRes["data"]
    
  def writeAll(self, output_path, outFormat):
    if not os.path.exists(output_path):
      os.mkdir(output_path)
  
    if outFormat=="npy":
      for i, dataItem in enumerate(self.data):
        for j, metaItem in enumerate(self.meta[i]):
          with open(os.path.join(output_path, metaItem["Modality"].lower()+str(j)+'.txt'), 'w') as outfile:
            outfile.write(dumps(metaItem))
    
        np.save(os.path.join(output_path, self.meta[i][0]["Modality"].lower()+'.npy'), dataItem, allow_pickle=True)

  @staticmethod
  def resample(meta, data, newSpacing):
    metaItem0 = meta[0]
    meta = copy.deepcopy(meta)
              
    zooming_vector = (
      metaItem0["PixelSpacing"][0] / newSpacing[0],
      metaItem0["PixelSpacing"][1] / newSpacing[1],
      metaItem0["SliceThickness"] / newSpacing[2])
        
    newData = zoom(data, zooming_vector)
        
    newShape = newData.shape
    newMetas = []
    for j in range(newShape[2]):
      newMeta = copy.deepcopy(metaItem0)
          
      newMeta["ImagePositionPatient"][0] = metaItem0["ImagePositionPatient"][0]
      newMeta["ImagePositionPatient"][1] = metaItem0["ImagePositionPatient"][1]
      newMeta["ImagePositionPatient"][2] = metaItem0["ImagePositionPatient"][2] - j*newSpacing[2]
          
      newMeta["PixelSpacing"] = [newSpacing[0], newSpacing[1]]
      newMeta["SliceThickness"] = newSpacing[2]
      newMeta["Rows"] = newShape[0]
      newMeta["Columns"] = newShape[1]
      newMeta["Slices"] = newShape[2] # Non standard
      newMeta["SliceLocation"] = newMeta["ImagePositionPatient"][2]
      
      newMetas.append(newMeta)
      
    return {"meta": newMetas, "data": newData}


def convertMetaToCollection(meta):
  #TODO Deal with numeric values and arrays
  col = {
    "ImagePositionPatient": list(map(float, meta.ImagePositionPatient)),
    "StudyDate": str(meta.StudyDate),
    "SeriesDate": str(meta.SeriesDate),
    "AcquisitionDate": str(meta.AcquisitionDate),
    #"ContentDate": str(meta.ContentDate),
    "Manufacturer": str(meta.Manufacturer),
    "PatientName": str(meta.PatientName),
    "PatientID": meta.PatientID,
    "PatientSex": str(meta.PatientSex),
    "PatientAge": str(meta.PatientAge),
    #"PatientWeight": meta.PatientWeight,
    "BodyPartExamined": str(meta.BodyPartExamined) if hasattr(meta, 'BodyPartExamined') else "",
    "SliceThickness": meta.SliceThickness,
    "ProtocolName": str(meta.ProtocolName),
    "PatientPosition": str(meta.PatientPosition),
    "StudyInstanceUID": str(meta.StudyInstanceUID),
    "SeriesInstanceUID": str(meta.SeriesInstanceUID),
    "ImageOrientationPatient": list(map(float, meta.ImageOrientationPatient)),
    "Rows": meta.Rows,
    "Columns": meta.Columns,
    "PixelSpacing": list(map(float, meta.PixelSpacing)),
    "BitsAllocated": meta.BitsAllocated,
    "BitsStored": meta.BitsStored,
    "HighBit": meta.HighBit,
    #"SmallestImagePixelValue": meta.SmallestImagePixelValue,
    #"LargestImagePixelValue": meta.LargestImagePixelValue,
    "RescaleIntercept": str(meta.RescaleIntercept) if hasattr(meta, 'RescaleIntercept') else "",
    "RescaleSlope": str(meta.RescaleSlope) if hasattr(meta, 'RescaleSlope') else "",
    "modalities": str(meta.Modality),
    "modality": str(meta.Modality),
    "Modality": str(meta.Modality),
    "SeriesDescription": str(meta.SeriesDescription) if hasattr(meta, 'SeriesDescription') else ""
  }
  return col
  
