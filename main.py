
if __name__ == "__main__":
  import sys, os
  from automation_api.api import DB
  from utils.DataUtils import DataUtils
  from utils.StructUtils import StructUtils
  from utils.mask2RT import mask2RT
  #from prediction.predictor import Predictor
  import copy
  import numpy as np
  
  
  dataPath = "/home/sylvain/Documents/Gitlab/automation/data" # For remote database
  dbName = 'automation3'
  
  series = ["/home/sylvain/Documents/Data/ANON242_CT_MR_Reg/ANON242_ANON242_CT_2019-05-10_081510_._Anonymized_n129__00000/1.2.752.243.1.1.20200505084909319.4790.22278.dcm",
    ] # Only 1 file per dcm series
    
    #"/home/sylvain/Documents/Data/ANON242_CT_MR_Reg/ANON242_ANON242_MR_2019-04-17_141409_Anonymized_Aligned.CT.20190510.08.15,.Anonymized_n129__00000/2.16.840.1.114362.1.6.5.9.16309.8719278761.546625978.600.18.dcm"
    
  seg = "/home/sylvain/Documents/Data/ANON242_CT_MR_Reg/ANON242_ANON242_RTst_2019-05-10_081510_._Anonymized_n1__00000/1.2.752.243.1.1.20200505084910772.5730.41446.dcm"
  
  dlConfig= {
    "gpu": 0,
    "model":  "./public/model/untrained_model.hdf5",
    "weights": "./public/model/weights.00100.hdf5",
    "pt_clip": [0, 10],
    "ct_clip": [-200, 200]
  }
  
  
  # Load initial data
  db = DB(dataPath=dataPath, dbName=dbName)
  
  seriesInfo = db.insertDcmSeries(series)
  segInfo = db.insertDcmStruct(seg, seriesInfo["meta"][0])
  
  seriesIds = seriesInfo["id"]
  seriesMeta = seriesInfo["meta"]
  seriesData = seriesInfo["data"]
  
  studyMeta = seriesMeta[0][0]
  
  segId = segInfo["id"]
  segMeta = segInfo["meta"]
  
  info = "Example: GTV autodelineation"
  label = "Initial data"
  rootId = db.insertWorkflowEntry(seriesIds, segId, None, studyMeta, info, None, label)
  
  
  # Preprocess data  
  x = []
  y = []
  z = []
  
  gtvFound = False
  for i, SegmentSequenceItem in enumerate(segMeta["StructureSetROISequence"]):
    if 'gtv' in SegmentSequenceItem["ROIName"].lower():
      for j, ContourSequenceItem in enumerate(segMeta["ROIContourSequence"][i]["ContourSequence"]):
        cData = ContourSequenceItem["ContourData"]
        x = np.concatenate((x, np.array(cData[0::3])))
        y = np.concatenate((y, np.array(cData[1::3])))
        z = np.concatenate((z, [cData[2]]))
        
      gtvFound = True
      print("GTV found: " + SegmentSequenceItem["ROIName"])
      break
  
  if not gtvFound:
    raise NameError('GTV not found in RTSTRUCT')
    
  xMean = np.mean(x)
  yMean = np.mean(y)
  zMean = np.mean(z)
  
  #TODO Ensure that volume is large enough. Otherwise, pad
  dataUtils = DataUtils()
  
  for i, data in enumerate(seriesData):
    dataUtils.appendDicomData(data, seriesMeta[i])
    
  dataUtils.resampleAll([1, 1, 1])
  dataUtils.cropAll(xMean-144/2, xMean+144/2, yMean-144/2, yMean+144/2, zMean-144/2, zMean+144/2)
  
  newMetaList = dataUtils.getMeta()
  newDataList = dataUtils.getData()
  
  print(len(newMetaList)) #!!!!!!!!!!! newMetaList is too long!
  
  ppSeriesIds = []
  for i, newData in enumerate(newDataList):
    ppSeriesInfo = db.insertData(newMetaList[i], newDataList[i])
    ppSeriesIds.append(ppSeriesInfo["id"])
  
  label = "Preprocessed data"
  ppWfId = db.insertWorkflowEntry(ppSeriesIds, None, None, studyMeta, info, [rootId], label)
  
  # Autodelineation
  #predictor = Predictor(dlConfig)
  #dlRes = predictor.run(ct=newDataList[0], pet=newDataList[1])
  #mask = dlRes["mask"]
  
  #for test
  mask = 2*np.ones(newDataList[0].shape, dtype="uint8")
  mask = mask[3:-3, 3:-3, 3:-3]
  mask = np.pad(mask, ((2, 2), (2, 2), (2, 2)), 'constant', constant_values=((0, 0), (0, 0), (0, 0)))
  
  # Postprocessing
  contourSequence = mask2RT(mask, newMetaList[0][0]["ImagePositionPatient"], [1, 1, 1])
  
  ppSegMeta = copy.deepcopy(segMeta)
  
  segItem = {
    "ROIDisplayColor": list(map(float, ppSegMeta["ROIContourSequence"][-1]["ROIDisplayColor"])),
    "ContourSequence": contourSequence
  }
  
  structureItem = {
    "ROINumber": len(ppSegMeta["StructureSetROISequence"])+1,
    "ROIName": "Box"
  }
  
  ppSegMeta["ROIContourSequence"].append(segItem)
  ppSegMeta["StructureSetROISequence"].append(structureItem)
  
  ppSegInfo = db.insertData(ppSegMeta)
  
  label = "Autodelineation"
  delinatId = db.insertWorkflowEntry(seriesIds, ppSegInfo["id"], None, studyMeta, info, [ppWfId], label)
  
  
  # User reviews
  label = "Sylvain"
  delinat = db.insertWorkflowEntry(seriesIds, ppSegInfo["id"], "sylvain", studyMeta, info, [delinatId], label)
  
  label = "Paul"
  delinat = db.insertWorkflowEntry(seriesIds, ppSegInfo["id"], "paul", studyMeta, info, [delinatId], label)
  
  label = "Ana"
  delinat = db.insertWorkflowEntry(seriesIds, ppSegInfo["id"], "ana", studyMeta, info, [delinatId], label)
  
