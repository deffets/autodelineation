
if __name__ == "__main__":
  from staple.staple import run_staple
  from automation_api.api import DB
  from utils.DataUtils import DataUtils
  from utils.StructUtils import StructUtils
  from utils.mask2RT import mask2RT
  from utils.RT2mask import RT2mask
  import copy
  import numpy as np
  
  dataPath = "/home/sylvain/Documents/Gitlab/automation/data" # For remote database
  dbName = 'automation3'
  
  workflowId = "5fc0e50eae3488abd02389c2"
  
  ROIName = "Box"
  
  
  db = DB(dataPath=dataPath, dbName=dbName)
  
  allEntries = db.getWorkflow(workflowId)
  
  ct = db.getSeries(allEntries[0]["seriesId"][0])
  gridSize = (ct["meta"][0]["Rows"], ct["meta"][0]["Columns"], len(ct["meta"]))
  ImagePositionPatient = ct["meta"][0]["ImagePositionPatient"]
  PixelSpacing = [ct["meta"][0]["PixelSpacing"][0], ct["meta"][0]["PixelSpacing"][1], ct["meta"][0]["SliceThickness"]]
    
  masks = []
  parentIds =[]
  for entry in allEntries:
    if len(entry["users"]):
      print("Found user entry: " + str(entry["users"]))
      print(entry["segId"])
      
      rt = db.getRT(entry["segId"])
      
      mask = RT2mask(rt, ROIName, gridSize, ImagePositionPatient, PixelSpacing)
      masks.append(mask)
      
      parentIds.append(str(entry["_id"]))
      
  stapleMask = run_staple(masks, verbose=True, convergence_threshold=None)

  contourSequence = mask2RT(masks[0], ImagePositionPatient, PixelSpacing)
  
  ppSegMeta = copy.deepcopy(rt)
  
  segItem = {
    "ROIDisplayColor": list(map(float, ppSegMeta["ROIContourSequence"][-1]["ROIDisplayColor"])),
    "ContourSequence": contourSequence
  }
  
  structureItem = {
    "ROINumber": len(ppSegMeta["StructureSetROISequence"])+1,
    "ROIName": "STAPLE"
  }
  
  ppSegMeta["ROIContourSequence"].append(segItem)
  ppSegMeta["StructureSetROISequence"].append(structureItem)
  
  ppSegInfo = db.insertData(ppSegMeta)
  print(ppSegInfo["id"])
  
  label = "STAPLE"
  studyMeta = ct["meta"][0]
  info = "STAPLE"
  delinat = db.insertWorkflowEntry(allEntries[0]["seriesId"], str(ppSegInfo["id"]), "sylvain", studyMeta, info, parentIds, label)
  
