from keras.models import Model, Sequential, load_model
from keras.layers import Input, concatenate, Conv3D, MaxPooling3D, Conv3DTranspose, Conv2D, MaxPooling2D, \
    Conv2DTranspose, Dropout, Flatten, Dense, ZeroPadding3D, Cropping3D, BatchNormalization, Activation
from keras.layers.advanced_activations import LeakyReLU, PReLU
import tensorflow as tf
from keras import backend as K
import numpy as np
from keras.optimizers import Adam
import keras
import os
from keras.callbacks import ModelCheckpoint, EarlyStopping
from math import log10, floor
import matplotlib.pyplot as plt
from scipy import ndimage
import pickle
from keras import regularizers
from sklearn.metrics import f1_score, confusion_matrix
from scipy.spatial.distance import directed_hausdorff, cdist
from keras.utils import np_utils


image_size = [144, 144, 144]

def dice_loss(y_true, y_pred):
    sh = tf.shape(y_true)
    y_true_f = tf.transpose(tf.reshape(y_true, [sh[0], sh[1] * sh[2] * sh[3]]))
    y_pred_f = tf.transpose(tf.reshape(y_pred, [sh[0], sh[1] * sh[2] * sh[3]]))
    intersection = tf.multiply(y_true_f, y_pred_f)
    intersection = tf.reduce_sum(intersection, 0)
    card_y_true = tf.reduce_sum(y_true_f, 0)
    card_y_pred = tf.reduce_sum(y_pred_f, 0)
    dices =  tf.math.divide(2 * intersection, card_y_true + card_y_pred)
    return -tf.reduce_mean(dices)

def dice_loss_smooth(y_true, y_pred):
    sh = tf.shape(y_true)
    y_true_f = tf.transpose(tf.reshape(y_true, [sh[0], sh[1] * sh[2] * sh[3]]))
    y_pred_f = tf.transpose(tf.reshape(y_pred, [sh[0], sh[1] * sh[2] * sh[3]]))
    intersection = tf.multiply(y_true_f, y_pred_f)
    intersection = tf.reduce_sum(intersection, 0)
    card_y_true = tf.reduce_sum(y_true_f, 0)
    card_y_pred = tf.reduce_sum(y_pred_f, 0)
    dices =  tf.math.divide(2 * intersection+1, card_y_true + card_y_pred+1)
    return -tf.reduce_mean(dices)


def unet_3d(params):
    nb_layers = len(params['feat_maps'])

    if params['modality'] in ['pet', 'ct']:
        n_input_channels = 1
    elif params['modality']=='dual':
        n_input_channels = 2
    else:
        print('ERROR: Unknown modality.')

    # Input layer
    inputs = Input(batch_shape=(None, *image_size, n_input_channels))

    # Encoding part
    skips = []
    x = inputs
    for block_num in range(nb_layers-1):
        nb_features = params['feat_maps'][block_num]
        x = Conv3D(nb_features, (3, 3, 3), activation='relu',padding='same')(x)
        x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)
        skips.append(x)
        x = MaxPooling3D(pool_size=(2, 2, 2))(x)

    # Bottleneck
    nb_features = params['feat_maps'][-1]
    x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)
    x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)

    # Decoding part
    for block_num in reversed(range(nb_layers-1)):
        nb_features = params['feat_maps'][block_num]
        x = concatenate([Conv3DTranspose(nb_features, (2, 2, 2), strides=(2, 2, 2), padding='same')(x),
                         skips[block_num]], axis=4)
        x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)
        x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)

    # Output layer
    outputs = Conv3D(1, (1, 1, 1), activation='sigmoid')(x)

    print('outputs.shape.dims', outputs.shape.dims)

    model = Model(inputs=[inputs], outputs=[outputs])

    if params['loss']=='dice_loss':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss)
    elif params['loss']=='dice_loss_smooth':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss_smooth)
    return model

    skips = []
    x = inputs
    for block_num in range(nb_layers-1):
        nb_features = params['feat_maps'][block_num]
        x = Conv3D(nb_features, (3, 3, 3), activation='relu',padding='same')(x)
        x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)
        skips.append(x)
        x = MaxPooling3D(pool_size=(2, 2, 2))(x)
        print(x)

    # Bottleneck
    nb_features = params['feat_maps'][-1]
    x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)
    x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)
    print(x)

    # Decoding part
    for block_num in reversed(range(nb_layers-1)):
        nb_features = params['feat_maps'][block_num]
        x = concatenate([Conv3DTranspose(nb_features, (2, 2, 2), strides=(2, 2, 2), padding='same')(x),
                         skips[block_num]], axis=4)
        x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)
        x = Conv3D(nb_features, (3, 3, 3), activation='relu', padding='same')(x)
        print(x)

def unet_3d_test1(params):
    if params['modality'] in ['pet', 'ct']:
        n_input_channels = 1
    elif params['modality']=='dual':
        n_input_channels = 2
    else:
        print('ERROR: Unknown modality.')
        
    inputs = Input(batch_shape=(None, *image_size, n_input_channels))


    conv1 = Conv3D(params['feat_maps'][0], (3, 3, 3), activation='relu', padding='same')(inputs)
    conv1 = Conv3D(params['feat_maps'][0], (3, 3, 3), activation='relu', padding='same')(conv1)
    pool1 = MaxPooling3D(pool_size=(2, 2, 2))(conv1)
    print(pool1)

    conv2 = Conv3D(params['feat_maps'][1], (3, 3, 3), activation='relu', padding='same')(pool1)
    conv2 = Conv3D(params['feat_maps'][1], (3, 3, 3), activation='relu', padding='same')(conv2)
    pool2 = MaxPooling3D(pool_size=(2, 2, 2))(conv2)
    print(pool2)

    conv3 = Conv3D(params['feat_maps'][2], (3, 3, 3), activation='relu', padding='same')(pool2)
    conv3 = Conv3D(params['feat_maps'][2], (3, 3, 3), activation='relu', padding='same')(conv3)
    pool3 = MaxPooling3D(pool_size=(2, 2, 2))(conv3)
    print(pool3)

    conv4 = Conv3D(params['feat_maps'][3], (3, 3, 3), activation='relu', padding='same')(pool3)
    conv4 = Conv3D(params['feat_maps'][3], (3, 3, 3), activation='relu', padding='same')(conv4)
    pool4 = MaxPooling3D(pool_size=(2, 2, 2))(conv4)
    print(pool4)

    conv5 = Conv3D(params['feat_maps'][-1], (3, 3, 3), activation='relu', padding='same')(pool4)
    conv5 = Conv3D(params['feat_maps'][-1], (3, 3, 3), activation='relu', padding='same')(conv5)
    print(conv5)

    up6 = concatenate(
        [Conv3DTranspose(params['feat_maps'][3], (2, 2, 2), strides=(2, 2, 2), padding='same')(conv5), conv4], axis=4)
    conv6 = Conv3D(params['feat_maps'][3], (3, 3, 3), activation='relu', padding='same')(up6)
    conv6 = Conv3D(params['feat_maps'][3], (3, 3, 3), activation='relu', padding='same')(conv6)
    print(conv6)

    up7 = concatenate(
        [Conv3DTranspose(params['feat_maps'][2], (2, 2, 2), strides=(2, 2, 2), padding='same')(conv6), conv3], axis=4)
    conv7 = Conv3D(params['feat_maps'][2], (3, 3, 3), activation='relu', padding='same')(up7)
    conv7 = Conv3D(params['feat_maps'][2], (3, 3, 3), activation='relu', padding='same')(conv7)
    print(conv7)

    up8 = concatenate(
        [Conv3DTranspose(params['feat_maps'][1], (2, 2, 2), strides=(2, 2, 2), padding='same')(conv7), conv2],
        axis=4)
    conv8 = Conv3D(params['feat_maps'][1], (3, 3, 3), activation='relu', padding='same')(up8)
    conv8 = Conv3D(params['feat_maps'][1], (3, 3, 3), activation='relu', padding='same')(conv8)
    
    up9 = concatenate(
        [Conv3DTranspose(params['feat_maps'][0], (2, 2, 2), strides=(2, 2, 2), padding='same')(conv8), conv1],
        axis=4)
    conv9 = Conv3D(params['feat_maps'][0], (3, 3, 3), activation='relu', padding='same')(up9)
    conv9 = Conv3D(params['feat_maps'][0], (3, 3, 3), activation='relu', padding='same')(conv9)

    

    # Output layer
    outputs = Conv3D(1, (1, 1, 1), activation='sigmoid')(conv9)

    model = Model(inputs=[inputs], outputs=[outputs])

    if params['loss']=='dice_loss':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss)
    elif params['loss']=='dice_loss_smooth':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss_smooth)
    return model

def unet_3d_test2(params):
    nb_layers = len(params['feat_maps'])
    dropout=0
    alpha_=0.05
    batchnorm=True

    if params['modality'] in ['pet', 'ct']:
        n_input_channels = 1
    elif params['modality']=='dual':
        n_input_channels = 2
    else:
        print('ERROR: Unknown modality.')

    # Input layer
    inputs = Input(batch_shape=(None, *image_size, n_input_channels))

    # Encoding part
    skips = []
    x = inputs
    for block_num in range(nb_layers-1):
        nb_features = params['feat_maps'][block_num]
        x = Conv3D(nb_features, (3, 3, 3), kernel_initializer='he_normal',padding='same')(x)
        x = BatchNormalization()(x) if batchnorm else x
        x = LeakyReLU(alpha=alpha_)(x)
        x = Dropout(rate=dropout)(x) if dropout > 0 else x
        
        x = Conv3D(nb_features, (3, 3, 3), kernel_initializer='he_normal',padding='same')(x)
        x = BatchNormalization()(x) if batchnorm else x
        x = LeakyReLU(alpha=alpha_)(x)
        x = Dropout(rate=dropout)(x) if dropout > 0 else x
        
        skips.append(x)
        
        x = MaxPooling3D(pool_size=(2, 2, 2))(x)

    # Bottleneck
    nb_features = params['feat_maps'][-1]
    
    x = Conv3D(nb_features, (3, 3, 3), kernel_initializer='he_normal',padding='same')(x)
    x = BatchNormalization()(x) if batchnorm else x
    x = LeakyReLU(alpha=alpha_)(x)
    x = Dropout(rate=dropout)(x) if dropout > 0 else x
    
    x = Conv3D(nb_features, (3, 3, 3), kernel_initializer='he_normal',padding='same')(x)
    x = BatchNormalization()(x) if batchnorm else x
    x = LeakyReLU(alpha=alpha_)(x)
    x = Dropout(rate=dropout)(x) if dropout > 0 else x

    # Decoding part
    for block_num in reversed(range(nb_layers-1)):
        nb_features = params['feat_maps'][block_num]
        x = concatenate([Conv3DTranspose(nb_features, (2, 2, 2), strides=(2, 2, 2), padding='same')(x),
                         skips[block_num]], axis=4)
        x = Conv3D(nb_features, (3, 3, 3), kernel_initializer='he_normal',padding='same')(x)
        x = BatchNormalization()(x) if batchnorm else x
        x = LeakyReLU(alpha=alpha_)(x)
        x = Dropout(rate=dropout)(x) if dropout > 0 else x
        
        x = Conv3D(nb_features, (3, 3, 3), kernel_initializer='he_normal',padding='same')(x)
        x = BatchNormalization()(x) if batchnorm else x
        x = LeakyReLU(alpha=alpha_)(x)
        x = Dropout(rate=dropout)(x) if dropout > 0 else x

    # Output layer
    outputs = Conv3D(1, (1, 1, 1), activation='sigmoid')(x)

    print('outputs.shape.dims', outputs.shape.dims)

    model = Model(inputs=[inputs], outputs=[outputs])

    if params['loss']=='dice_loss':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss)
    elif params['loss']=='dice_loss_smooth':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss_smooth)
    return model

def unet_3d_pad(params):
    dropout=0
    alpha_=0.05
    batchnorm=True
    kernel_initializer_='he_normal'
    
    if params['modality'] in ['pet', 'ct']:
        n_input_channels = 1
    elif params['modality']=='dual':
        n_input_channels = 2
    else:
        print('ERROR: Unknown modality.')
        
    inputs = Input(batch_shape=(None, *image_size, n_input_channels))


    conv1 = Conv3D(params['feat_maps'][0], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(inputs)
    conv1 = BatchNormalization()(conv1) if batchnorm else conv1
    conv1 = LeakyReLU(alpha=alpha_)(conv1)
    conv1 = Dropout(rate=dropout)(conv1) if dropout > 0 else conv1
    conv1 = Conv3D(params['feat_maps'][0], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv1)
    conv1 = BatchNormalization()(conv1) if batchnorm else conv1
    conv1 = LeakyReLU(alpha=alpha_)(conv1)
    conv1 = Dropout(rate=dropout)(conv1) if dropout > 0 else conv1
    pool1 = MaxPooling3D(pool_size=(2, 2, 2))(conv1)

    conv2 = Conv3D(params['feat_maps'][1], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(pool1)
    conv2 = BatchNormalization()(conv2) if batchnorm else conv2
    conv2 = LeakyReLU(alpha=alpha_)(conv2)
    conv2 = Dropout(rate=dropout)(conv2) if dropout > 0 else conv2
    conv2 = Conv3D(params['feat_maps'][1] * 2, (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv2)
    conv2 = BatchNormalization()(conv2) if batchnorm else conv2
    conv2 = LeakyReLU(alpha=alpha_)(conv2)
    conv2 = Dropout(rate=dropout)(conv2) if dropout > 0 else conv2
    pool2 = MaxPooling3D(pool_size=(2, 2, 2))(conv2)

    conv3 = Conv3D(params['feat_maps'][2], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(pool2)
    conv3 = BatchNormalization()(conv3) if batchnorm else conv3
    conv3 = LeakyReLU(alpha=alpha_)(conv3)
    conv3 = Dropout(rate=dropout)(x) if dropout > 0 else conv3
    conv3 = Conv3D(params['feat_maps'][2], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv3)
    conv3 = BatchNormalization()(conv3) if batchnorm else conv3
    conv3 = LeakyReLU(alpha=alpha_)(conv3)
    conv3 = Dropout(rate=dropout)(x) if dropout > 0 else conv3
    pool3 = MaxPooling3D(pool_size=(2, 2, 2))(conv3)

    conv4 = Conv3D(params['feat_maps'][3], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(pool3)
    conv4 = BatchNormalization()(conv4) if batchnorm else conv4
    conv4 = LeakyReLU(alpha=alpha_)(conv4)
    conv4 = Dropout(rate=dropout)(conv4) if dropout > 0 else conv4
    conv4 = Conv3D(params['feat_maps'][3], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv4)
    conv4 = BatchNormalization()(conv4) if batchnorm else conv4
    conv4 = LeakyReLU(alpha=alpha_)(conv4)
    conv4 = Dropout(rate=dropout)(conv4) if dropout > 0 else conv4
    pool4 = MaxPooling3D(pool_size=(2, 2, 2))(conv4)

    conv5 = Conv3D(params['feat_maps'][4], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(pool4)
    conv5 = BatchNormalization()(conv5) if batchnorm else conv5
    conv5 = LeakyReLU(alpha=alpha_)(conv5)
    conv5 = Dropout(rate=dropout)(conv5) if dropout > 0 else conv5
    conv5 = Conv3D(params['feat_maps'][4], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv5)
    conv5 = BatchNormalization()(conv5) if batchnorm else conv5
    conv5 = LeakyReLU(alpha=alpha_)(conv5)
    conv5 = Dropout(rate=dropout)(conv5) if dropout > 0 else conv5
    conv5 = ZeroPadding3D(padding=((0,1),(0,1),(0,1)))(conv5)
    pool5 = MaxPooling3D(pool_size=(2, 2, 2))(conv5)

    conv6 = Conv3D(params['feat_maps'][5], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(pool5)
    conv6 = Conv3D(params['feat_maps'][5], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv6)

    up7 = concatenate(
        [Conv3DTranspose(params['feat_maps'][4], (2, 2, 2), strides=(2, 2, 2), padding='same')(conv6), conv5],axis=4)
    up7 = Cropping3D(cropping=((0,1),(0,1),(0,1)))(up7)
    conv7 = Conv3D(params['feat_maps'][4], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(up7)
    conv7 = BatchNormalization()(conv7) if batchnorm else conv7
    conv7 = LeakyReLU(alpha=alpha_)(conv7)
    conv7 = Dropout(rate=dropout)(conv7) if dropout > 0 else conv7
    conv7 = Conv3D(params['feat_maps'][4], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv7)
    conv7 = BatchNormalization()(conv7) if batchnorm else conv7
    conv7 = LeakyReLU(alpha=alpha_)(conv7)
    conv7 = Dropout(rate=dropout)(conv7) if dropout > 0 else conv7

    up8 = concatenate(
        [Conv3DTranspose(params['feat_maps'][3], (2, 2, 2), strides=(2, 2, 2), padding='same')(conv7), conv4],
        axis=4)
    conv8 = Conv3D(params['feat_maps'][3], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(up8)
    conv8 = BatchNormalization()(conv8) if batchnorm else conv8
    conv8 = LeakyReLU(alpha=alpha_)(conv8)
    conv8 = Dropout(rate=dropout)(conv8) if dropout > 0 else conv8
    conv8 = Conv3D(params['feat_maps'][3], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv8)

    up9 = concatenate(
        [Conv3DTranspose(params['feat_maps'][2], (2, 2, 2), strides=(2, 2, 2), padding='same')(conv8), conv3],
        axis=4)
    conv9 = Conv3D(params['feat_maps'][2], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(up9)
    conv9 = BatchNormalization()(conv9) if batchnorm else conv9
    conv9 = LeakyReLU(alpha=alpha_)(conv9)
    conv9 = Dropout(rate=dropout)(conv9) if dropout > 0 else conv9   
    conv9 = Conv3D(params['feat_maps'][2], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv9)
    conv9 = BatchNormalization()(conv9) if batchnorm else conv9
    conv9 = LeakyReLU(alpha=alpha_)(conv9)
    conv9 = Dropout(rate=dropout)(conv9) if dropout > 0 else conv9

    up10 = concatenate(
        [Conv3DTranspose(params['feat_maps'][1], (2, 2, 2), strides=(2, 2, 2), padding='same')(conv9), conv2],
        axis=4)
    conv10 = Conv3D(params['feat_maps'][1], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(up10)
    conv10 = BatchNormalization()(conv10) if batchnorm else conv10
    conv10 = LeakyReLU(alpha=alpha_)(conv10)
    conv10 = Dropout(rate=dropout)(conv10) if dropout > 0 else conv10
    conv10 = Conv3D(params['feat_maps'][1], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv10)
    conv10 = BatchNormalization()(conv10) if batchnorm else conv10
    conv10 = LeakyReLU(alpha=alpha_)(conv10)
    conv10 = Dropout(rate=dropout)(conv10) if dropout > 0 else conv10

    up11 = concatenate(
        [Conv3DTranspose(params['feat_maps'][0], (2, 2, 2), strides=(2, 2, 2), padding='same')(conv10), conv1],
        axis=4)
    conv11 = Conv3D(params['feat_maps'][0], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(up11)
    conv11 = BatchNormalization()(conv11) if batchnorm else conv11
    conv11 = LeakyReLU(alpha=alpha_)(conv11)
    conv11 = Dropout(rate=dropout)(conv11) if dropout > 0 else conv11
    conv11 = Conv3D(params['feat_maps'][0], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv11)
    conv11 = BatchNormalization()(conv11) if batchnorm else conv11
    conv11 = LeakyReLU(alpha=alpha_)(conv11)
    conv11 = Dropout(rate=dropout)(conv11) if dropout > 0 else conv11

    # Output layer
    outputs = Conv3D(1, (1, 1, 1), activation='sigmoid')(conv11)

    model = Model(inputs=[inputs], outputs=[outputs])

    if params['loss']=='dice_loss':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss)
    elif params['loss']=='dice_loss_smooth':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss_smooth)
    return model

#https://github.com/xiehousen/Dense-Unet
def unet_3d_dense(params):
    if params['modality'] in ['pet', 'ct']:
        n_input_channels = 1
    elif params['modality']=='dual':
        n_input_channels = 2
    else:
        print('ERROR: Unknown modality.')
        
    dropout=0
    alpha_=0.05
    batchnorm=True
    kernel_initializer_='he_normal'
        
    inputs = Input(batch_shape=(None, *image_size, n_input_channels))

    conv1_1 = Conv3D(params['feat_maps'][0], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(inputs)
    conv1_1 = BatchNormalization()(conv1_1) if batchnorm else conv1_1
    conv1_1 = LeakyReLU(alpha=alpha_)(conv1_1)
    conv1_2 = Conv3D(params['feat_maps'][0], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv1_1)
    conv1_2 = BatchNormalization()(conv1_2) if batchnorm else conv1_2
    conv1_2 = LeakyReLU(alpha=alpha_)(conv1_2)
    merge1 = concatenate([conv1_1,conv1_2], axis = 4)
    pool1=MaxPooling3D(pool_size=(2, 2, 2))(merge1)
    print(pool1)
    
    conv2_1 = Conv3D(params['feat_maps'][1], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(pool1)
    conv2_1 = BatchNormalization()(conv2_1) if batchnorm else conv2_1
    conv2_1 = LeakyReLU(alpha=alpha_)(conv2_1)
    conv2_2 = Conv3D(params['feat_maps'][1], (3, 3, 3),  kernel_initializer=kernel_initializer_, padding='same')(conv2_1)
    conv2_2 = BatchNormalization()(conv2_2) if batchnorm else conv2_2
    conv2_2 = LeakyReLU(alpha=alpha_)(conv2_2)
    merge2 = concatenate([conv2_1,conv2_2], axis = 4)
    pool2=MaxPooling3D(pool_size=(2, 2, 2))(merge2)
    print(pool2)
    
    conv3_1 = Conv3D(params['feat_maps'][2], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(pool2)
    conv3_1 = BatchNormalization()(conv3_1) if batchnorm else conv3_1
    conv3_1 = LeakyReLU(alpha=alpha_)(conv3_1)
    conv3_2 = Conv3D(params['feat_maps'][2], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv3_1)
    conv3_2 = BatchNormalization()(conv3_2) if batchnorm else conv3_2
    conv3_2 = LeakyReLU(alpha=alpha_)(conv3_2)
    merge3 = concatenate([conv3_1,conv3_2], axis = 4)
    pool3=MaxPooling3D(pool_size=(2, 2, 2))(merge3)
    print(pool3)
    
    conv4_1 = Conv3D(params['feat_maps'][3], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(pool3)
    conv4_1 = BatchNormalization()(conv4_1) if batchnorm else conv4_1
    conv4_1 = LeakyReLU(alpha=alpha_)(conv4_1)
    conv4_2 = Conv3D(params['feat_maps'][3], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv4_1)
    conv4_2 = BatchNormalization()(conv4_2) if batchnorm else conv4_2
    conv4_2 = LeakyReLU(alpha=alpha_)(conv4_2)
    merge4 = concatenate([conv4_1,conv4_2], axis = 4)
    pool4=MaxPooling3D(pool_size=(2, 2, 2))(merge4)
    print(pool4)
    
    conv5_1 = Conv3D(params['feat_maps'][-1], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(pool4)
    conv5_1 = BatchNormalization()(conv5_1) if batchnorm else conv5_1
    conv5_1 = LeakyReLU(alpha=alpha_)(conv5_1)
    conv5_2= Conv3D(params['feat_maps'][-1], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv5_1)
    conv5_2 = BatchNormalization()(conv5_2) if batchnorm else conv5_2
    conv5_2 = LeakyReLU(alpha=alpha_)(conv5_2)
    merge5=concatenate([conv5_1,conv5_2], axis = 4)
    print(merge5)
    
    up6 = concatenate(
        [Conv3DTranspose(params['feat_maps'][3], (2, 2, 2), strides=(2, 2, 2), padding='same')(merge5), pool3], axis=4)
    
    conv6_1 = Conv3D(params['feat_maps'][3], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(up6)
    conv6_1 = BatchNormalization()(conv6_1) if batchnorm else conv6_1
    conv6_1 = LeakyReLU(alpha=alpha_)(conv6_1)
    conv6_2 = Conv3D(params['feat_maps'][3], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv6_1)
    conv6_2 = BatchNormalization()(conv6_2) if batchnorm else conv6_2
    conv6_2 = LeakyReLU(alpha=alpha_)(conv6_2)
    merge6 = concatenate([conv6_1,conv6_2], axis = 4)
    print(merge6)
    
    up7 = concatenate(
        [Conv3DTranspose(params['feat_maps'][2], (2, 2, 2), strides=(2, 2, 2), padding='same')(merge6), pool2], axis=4)
    
    conv7_1 = Conv3D(params['feat_maps'][2], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(up7)
    conv7_1 = BatchNormalization()(conv7_1) if batchnorm else conv7_1
    conv7_1 = LeakyReLU(alpha=alpha_)(conv7_1)
    conv7_2 = Conv3D(params['feat_maps'][2], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv7_1)
    conv7_2 = BatchNormalization()(conv7_2) if batchnorm else conv7_2
    conv7_2 = LeakyReLU(alpha=alpha_)(conv7_2)
    merge7 = concatenate([conv7_1,conv7_2], axis = 4)
    print(merge7)
    
    up8 = concatenate(
        [Conv3DTranspose(params['feat_maps'][1], (2, 2, 2), strides=(2, 2, 2), padding='same')(merge7), pool1], axis=4)
    
    conv8_1 = Conv3D(params['feat_maps'][1], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(up8)
    conv8_1 = BatchNormalization()(conv8_1) if batchnorm else conv8_1
    conv8_1 = LeakyReLU(alpha=alpha_)(conv8_1)
    conv8_2 = Conv3D(params['feat_maps'][1], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv8_1)
    conv8_2 = BatchNormalization()(conv8_2) if batchnorm else conv8_2
    conv8_2 = LeakyReLU(alpha=alpha_)(conv8_2)
    merge8 = concatenate([conv8_1,conv8_2], axis = 4)
    print(merge8)
    
    up9 = concatenate(
        [Conv3DTranspose(params['feat_maps'][0], (2, 2, 2), strides=(2, 2, 2), kernel_initializer=kernel_initializer_, padding='same')(merge8), merge1], axis=4)
    
    conv9_1 = Conv3D(params['feat_maps'][0], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(up9)
    conv9_1 = BatchNormalization()(conv9_1) if batchnorm else conv9_1
    conv9_1 = LeakyReLU(alpha=alpha_)(conv9_1)
    conv9_2 = Conv3D(params['feat_maps'][0], (3, 3, 3), kernel_initializer=kernel_initializer_, padding='same')(conv9_1)
    conv9_2 = BatchNormalization()(conv9_2) if batchnorm else conv9_2
    conv9_2 = LeakyReLU(alpha=alpha_)(conv9_2)
    merge9 = concatenate([conv9_1,conv9_2], axis = 4)
    print(merge9)
    
    conv9 = Conv3D(2, (3, 3, 3), padding='same')(merge9)
    conv9 = BatchNormalization()(conv9) if batchnorm else conv9
    conv9 = LeakyReLU(alpha=alpha_)(conv9)
   
    print(conv9)

    # Output layer
    outputs = Conv3D(1, (1, 1, 1), activation='sigmoid')(conv9)


    model = Model(inputs=[inputs], outputs=[outputs])

    if params['loss']=='dice_loss':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss)
    elif params['loss']=='dice_loss_smooth':
        model.compile(optimizer=Adam(params['lr']), loss=dice_loss_smooth)
    return model

