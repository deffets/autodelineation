
from keras.models import load_model
import numpy as np
import os
from .models import dice_loss_smooth

class Predictor:
  def __init__(self, config):
    self.modelName = config["model"]
    self.weightsName = config["weights"]
    
    self.gpu = config["gpu"]
    
    self.ct_clip_values = config["ct_clip"]
    self.pt_clip_values = config["pt_clip"]
    
    print('Loading model...')
    self.model = load_model(self.modelName, custom_objects={'dice_loss_smooth': dice_loss_smooth})
    self.model.load_weights(self.weightsName)
    
    print('Done.')
    
  def run(self, pet=None, ct=None):
    os.environ["CUDA_VISIBLE_DEVICES"] = str(self.gpu)
    
    if pet:
      pet = np.expand_dims(pet, axis=0)
      # pet = np.expand_dims(pet, axis=-1)
      pet = np.clip(pet, self.pt_clip_values[0], self.pt_clip_values[1])
      vmin = np.min(pet)
      vmax = np.max(pet)
      pet = (pet - vmin) / (vmax - vmin)
      #pet=(pet-np.mean(pet))/np.std(pet)
                
    if ct:
      image_size = ct.shape
      
      print(ct)
      ct = np.expand_dims(ct, axis=0)
      # ct = np.expand_dims(ct, axis=-1)
      ct = np.clip(ct, self.ct_clip_values[0], self.ct_clip_values[1])
      vmin = np.min(ct)
      vmax = np.max(ct)
      ct = (ct - vmin) / (vmax - vmin)
      #ct=(ct-np.mean(ct))/np.std(ct)

    if len(modalities) == 1 and 'ct' in modalities:
      X = np.zeros((1,*image_size,2))
      X[0, :, :, :, 0] = ct
      X[0, :, :, :, 1] = ct
    elif len(modalities) == 2:
      X = np.zeros((1,*image_size,2))
      X[0, :, :, :, 0] = pet
      X[0, :, :, :, 1] = ct
    else:
      print('ERROR.')
      return

    prediction = self.model.predict(X)
    prediction = prediction[0, :, :, :, 0]
    prediction_mask = (prediction>0.5)
    
    return {"mask": prediction_mask, "proba": prediction}
  
